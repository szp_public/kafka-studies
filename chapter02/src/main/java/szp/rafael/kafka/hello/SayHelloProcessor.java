package szp.rafael.kafka.hello;

import org.apache.kafka.streams.processor.api.Processor;
import org.apache.kafka.streams.processor.api.ProcessorContext;
import org.apache.kafka.streams.processor.api.Record;

public class SayHelloProcessor implements Processor<Void, String, Void, Void> {

  @Override
  public void init(ProcessorContext<Void, Void> context) {
    Processor.super.init(context);
  }

  @Override
  public void process(Record<Void, String> record) {
    System.out.printf("(Processor API) Hello, %s\n", record.value());
  }

  @Override
  public void close() {
    Processor.super.close();
  }
}
