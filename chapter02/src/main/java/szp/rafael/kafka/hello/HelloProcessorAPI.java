package szp.rafael.kafka.hello;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.Topology;
import szp.rafael.kafka.common.config.StreamsConfiguration;

public class HelloProcessorAPI {
  public static void main(String[] args) {
    System.out.println("Processor API");

    Topology topology = new Topology();

    topology.addSource("UserSource", "users");
    topology.addProcessor("SayHello", SayHelloProcessor::new, "UserSource");

    KafkaStreams streams = new KafkaStreams(topology, StreamsConfiguration.properties("dev2"));
    streams.start();
    Runtime.getRuntime().addShutdownHook(new Thread(streams::close));

  }
}
