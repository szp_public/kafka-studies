package szp.rafael.kafka.hello;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import szp.rafael.kafka.common.config.StreamsConfiguration;

public class HelloDSL {
  public static void main(String[] args) {

    System.out.println("DSL");

    StreamsBuilder builder = new StreamsBuilder();

    KStream<Void, String> stream = builder.stream("users");
    stream.foreach((key, value) -> System.out.println("(DSL) Hello, " + value));

    KafkaStreams streams = new KafkaStreams(builder.build(), StreamsConfiguration.properties("dev1"));
    streams.start();

    Runtime.getRuntime().addShutdownHook(new Thread(streams::close));

  }
}
