# Chapter 02

## reqs

```shell
#where 'broker' is the container name or hash
docker exec -it broker bash
kafka-topics --bootstrap-server localhost:9092 --create --topic users --replication-factor 1 --partitions 4
```

## Producing messages

~~~shell
#where 'broker' is the container name or hash
docker exec -it broker bash
kafka-console-producer --bootstrap-server localhost:9092 --topic users

#then type any name than `Enter` key
~~~

## Running examples

**DSL example**

```shell
mvn compile exec:java -PDSL
```

**Processor API example**

```shell
mvn compile exec:java -PProcessorAPI
```