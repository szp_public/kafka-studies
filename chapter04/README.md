# chapter 04

## Req scripts

Score events
```shell
docker exec -it broker bash
kafka-topics \
  --bootstrap-server localhost:9092 \
  --topic score-events \
  --replication-factor 1 \
  --partitions 4 \
  --create
```

Players

```shell
kafka-topics \
  --bootstrap-server localhost:9092 \
  --topic players \
  --replication-factor 1 \
  --partitions 4 \
  --create
```

Products
```shell
kafka-topics \
  --bootstrap-server localhost:9092 \
  --topic products \
  --replication-factor 1 \
  --partitions 4 \
  --create
```

## Producing

```shell
kafka-console-producer --bootstrap-server localhost:9092 --topic players --property "parse.key=true" --property "key.separator=|"  < /tmp/players.kjson
kafka-console-producer --bootstrap-server localhost:9092 --topic products --property "parse.key=true" --property "key.separator=|"  < /tmp/products.kjson
kafka-console-producer --bootstrap-server localhost:9092 --topic score-events < /tmp/score-events.kjson 
```

