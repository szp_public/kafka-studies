package szp.rafael.kafka.chapter04.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.test.TestRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import szp.rafael.kafka.chapter04.model.Player;
import szp.rafael.kafka.chapter04.model.Product;
import szp.rafael.kafka.chapter04.model.aggregation.HighScores;
import szp.rafael.kafka.chapter04.model.join.ScoreEvent;
import szp.rafael.kafka.chapter04.serialization.JsonSerdes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class LeaderboardTopologyTest {

  TopologyTestDriver testDriver;
  TestInputTopic<byte[], ScoreEvent> inputScoreEventTopic;
  Random random = new Random();
  TestInputTopic<String, Product> inputProductTopic;
  TestInputTopic<String, Player> inputPlayerTopic;
  TestOutputTopic<String, HighScores> outputTopic;


  @BeforeEach
  void setup() {
    Topology topology = LeaderboardTopology.build();

    Properties props = new Properties();
    props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "nono:1523");
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
    props.put(StreamsConfig.STATE_DIR_CONFIG, System.getProperty("java.io.tmpdir")+ File.separator+"test-"+Math.abs(random.nextLong()));
    testDriver = new TopologyTestDriver(topology, props);

    inputScoreEventTopic = testDriver.createInputTopic(StreamConstants.SCORE_EVENTS_STREAM, Serdes.ByteArray().serializer(), JsonSerdes.ScoreEvent().serializer());
    inputPlayerTopic = testDriver.createInputTopic(StreamConstants.PLAYERS_STREAM,Serdes.String().serializer(),JsonSerdes.Player().serializer());
    inputProductTopic = testDriver.createInputTopic(StreamConstants.PRODUCTS_STREAM,Serdes.String().serializer(),JsonSerdes.Product().serializer());

    outputTopic = testDriver.createOutputTopic(StreamConstants.HIGH_SCORES_STREAM, Serdes.String().deserializer(), JsonSerdes.HighScores().deserializer());
  }

  @Test
  void should_input_score_events() throws InterruptedException {
    ScoreEvent scoreEvent = ScoreEvent.builder().score(1.0).playerId(1L).productId(1L).build();
    Product product = Product.builder().id(1L).name("test-product").build();
    Player player = Player.builder().id(1L).name("test-player").build();
    List<KeyValue<byte[],ScoreEvent>> scoreEvents = new ArrayList<>();
    for(long i=0L; i<10L;i++){
      scoreEvents.add(new KeyValue<byte[],ScoreEvent>(null,ScoreEvent.builder()
        .playerId(Long.valueOf(random.nextInt(4)+1))
        .productId(1L)
        .score(random.nextDouble()*10)
        .build()
      ));
    }
    inputScoreEventTopic.pipeInput(scoreEvent);
    inputProductTopic.pipeInput(String.valueOf(product.getId()),product);
    List<KeyValue<String,Player>> players = List.of(
      new KeyValue<>("4",Player.builder().id(4L).name("teste-4").build()),
      new KeyValue<>("3",Player.builder().id(3L).name("teste-3").build()),
      new KeyValue<>("2",Player.builder().id(2L).name("teste-2").build()),
      new KeyValue<>("1",player));
    inputPlayerTopic.pipeKeyValueList(players);
    inputScoreEventTopic.pipeKeyValueList(scoreEvents);

    assertFalse(outputTopic.isEmpty());

    System.out.println(outputTopic.isEmpty());
    System.out.println(outputTopic.readRecordsToList().stream().count());
  }


}