package szp.rafael.kafka.chapter04.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

  @Test
  void should_build_new_player_with_lombok(){
    Player p = Player.builder().id(0L).name("teste").build();
    assertEquals("teste",p.getName());
    assertEquals(0L,p.getId());
  }

}