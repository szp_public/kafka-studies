package szp.rafael.kafka.chapter04.model.aggregation;

import lombok.Data;
import szp.rafael.kafka.chapter04.model.join.Enriched;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

@Data
public class HighScores {

  private TreeSet<Enriched> highScores = new TreeSet();

  public HighScores add(final Enriched enriched) {
    highScores.add(enriched);
    if (highScores.size() > 3) {
      highScores.remove(highScores.last());
    }
    return this;
  }

  public List<Enriched> toList() {
    List<Enriched> playerScores = new ArrayList<>();
    playerScores.addAll(highScores);
    return playerScores;
  }


}
