package szp.rafael.kafka.chapter04.service;

import io.javalin.Javalin;
import io.javalin.http.Context;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyQueryMetadata;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.*;
import szp.rafael.kafka.chapter04.model.aggregation.HighScores;
import szp.rafael.kafka.chapter04.model.join.Enriched;
import szp.rafael.kafka.chapter04.stream.StreamConstants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class LeaderboardService {

  protected HostInfo hostInfo;
  protected  KafkaStreams streams;

  public LeaderboardService(HostInfo hostInfo, KafkaStreams streams) {
    this.hostInfo = hostInfo;
    this.streams = streams;
  }

  public void start(){
    Javalin app = Javalin.create().start(hostInfo.port());

    app.get("/leaderboard", this::getAll);
    app.get("/leaderboard/count",this::countAll);
    app.get("/leaderboard/count/local",this::countLocal);
    app.get("/leaderboard/:key",this::getByKey);
  }

  private void countLocal(Context context) {
    long count = 0L;
    try{
      count = getStore().approximateNumEntries();
    }catch(Exception e){
      System.err.println("Error on getting count "+e.getMessage());
      e.printStackTrace();
    }finally {
      context.result(String.valueOf(count));
    }
  }

  private void countAll(Context context) {
    //retrieving the local count
    AtomicReference<Long> count = new AtomicReference<>(getStore().approximateNumEntries());

    //retrieving count from other consumers
    streams.allMetadataForStore(StreamConstants.LEADER_BOARDS_STORE).forEach((metadata)->{
      //ignoring local store because its already on the count
      if(!hostInfo.equals(metadata.hostInfo())){
        count.updateAndGet(v -> v + countFromRemote(metadata.hostInfo().host(), metadata.hostInfo().port()));
      }
    });

    context.json(count);
  }

  private Long countFromRemote(String host, int port) {
    long count = 0L;
    OkHttpClient client = new OkHttpClient();
    String url = String.format("http://%s:%s/leaderboard/count/local",host,port);
    Request request = new Request.Builder().url(url).build();
    try(Response execute = client.newCall(request).execute()){
      count = Long.parseLong(execute.body().toString());
    }catch (Exception e){
      System.err.println(e.getMessage());
      e.printStackTrace();
    }finally{
      return count;
    }
  }

  private ReadOnlyKeyValueStore<String, HighScores> getStore(){
    return this.streams.store(
      StoreQueryParameters.fromNameAndType(StreamConstants.LEADER_BOARDS_STORE, QueryableStoreTypes.keyValueStore())
    );
  }

  private void getAll(Context context) {
    Map<String, List<Enriched>> leaderboard = new HashMap<>();
    try(final KeyValueIterator<String, HighScores> range = this.getStore().all()){
      while(range.hasNext()){
        final KeyValue<String, HighScores> next = range.next();
        String key = next.key;
        final HighScores hs = next.value;
        leaderboard.put(key,hs.toList());
      }
    }
    context.json(leaderboard);
  }

  void getByKey(Context ctx) {
    String productId = ctx.pathParam("key");

    // find out which host has the key
    KeyQueryMetadata metadata =
      streams.queryMetadataForKey(StreamConstants.LEADER_BOARDS_STORE, productId, Serdes.String().serializer());

    // the local instance has this key
    if (hostInfo.equals(metadata.activeHost())) {
      System.out.println("Querying local store for key");
      HighScores highScores = getStore().get(productId);

      if (highScores == null) {
        // game was not found
        ctx.status(404);
        return;
      }

      // game was found, so return the high scores
      ctx.json(highScores.toList());
      return;
    }

    // a remote instance has the key
    String remoteHost = metadata.activeHost().host();
    int remotePort = metadata.activeHost().port();
    String url =
      String.format(
        "http://%s:%d/leaderboard/%s",
        // params
        remoteHost, remotePort, productId);

    // issue the request
    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder().url(url).build();

    try (Response response = client.newCall(request).execute()) {
      System.out.println("Querying remote store for key");
      ctx.result(response.body().string());
    } catch (Exception e) {
      ctx.status(500);
    }
  }



}
