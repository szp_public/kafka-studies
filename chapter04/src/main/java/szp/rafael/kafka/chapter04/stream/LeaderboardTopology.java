package szp.rafael.kafka.chapter04.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import szp.rafael.kafka.chapter04.model.*;
import szp.rafael.kafka.chapter04.model.aggregation.HighScores;
import szp.rafael.kafka.chapter04.model.join.Enriched;
import szp.rafael.kafka.chapter04.model.join.ScoreEvent;
import szp.rafael.kafka.chapter04.model.join.ScoreWithPlayer;
import szp.rafael.kafka.chapter04.serialization.JsonSerdes;

public class LeaderboardTopology {

  public static Topology build() {

    StreamsBuilder builder = new StreamsBuilder();

    //Source
    //V1: KStream<byte[], ScoreEvent> , v2: KStream<String, ScoreEvent>
    KStream<String, ScoreEvent> scoreEvents = builder.stream(StreamConstants.SCORE_EVENTS_STREAM,
      Consumed.with(Serdes.ByteArray(), JsonSerdes.ScoreEvent()))
      //Marked for repartition on selecting a new key
      //When selecting a new key, all related keys go to the same partition. See figure 4-6 (pg 116) of the book
      .selectKey((k, v) -> v.getPlayerId().toString());

    scoreEvents.print(Printed.<String,ScoreEvent>toSysOut().withLabel("score-events-stream"));

    final KTable<String, Player> players = builder.table(StreamConstants.PLAYERS_STREAM, Consumed.with(Serdes.String(), JsonSerdes.Player()));


    //Every local task will have a copy of the products table
    final GlobalKTable<String, Product> products = builder.globalTable(StreamConstants.PRODUCTS_STREAM, Consumed.with(Serdes.String(), JsonSerdes.Product()));

    // JOINS Structure
    final ValueJoiner<ScoreEvent, Player, ScoreWithPlayer> scorePlayerJoiner = (score, player) -> new ScoreWithPlayer(score, player);
    final Joined<String, ScoreEvent, Player> playerJoinParams = Joined.with(Serdes.String(), JsonSerdes.ScoreEvent(), JsonSerdes.Player());

    final ValueJoiner<ScoreWithPlayer, Product, Enriched> productJoiner = ((scoreWithPlayer, product) -> new Enriched(scoreWithPlayer, product));
    final KeyValueMapper<String, ScoreWithPlayer, String> keyMapper = (lKey, scoreWithPlayer) -> String.valueOf(scoreWithPlayer.getScoreEvent().getProductId());

    //Joining
    //Telling that the rekeyed scoreEvents  will be joined with the players when scoreEvents.key == players.key
    final KStream<String, ScoreWithPlayer> withPlayers = scoreEvents.join(players, scorePlayerJoiner, playerJoinParams);
    withPlayers.print(Printed.<String,ScoreWithPlayer>toSysOut().withLabel("with-players-stream"));
    //Telling joining is done when keymapper(products.key == ScoreWithPlayer.score.productId)
    //The Joined instance is replayed by KeyValueMapper because there is a copy of the products GlobalTable in every local task
    final KStream<String, Enriched> withProducts = withPlayers.join(products, keyMapper, productJoiner);
    withProducts.print(Printed.<String,Enriched>toSysOut().withLabel("with-products-stream"));

    //Grouping
    //The grouping is needed similarly as the scoredEvents. See figure 4-6 (pg 116) of the book
    //Since the the key of the Enriched is the PlayerId, we need to get the high scores by products
    //If we need a high scores by players we could use groupByKey instead
    final KGroupedStream<String, Enriched> grouped = withProducts.groupBy(
      (key, enriched) -> enriched.getProductId().toString(), //Grouping by productId
      Grouped.with(Serdes.String(), JsonSerdes.Enriched()) // Telling how the serialization is done
    );


    //Agreggating
    //Getting the top 3 scores
    final KTable<String, HighScores> highScores = grouped.aggregate(
      HighScores::new,
      (key, enriched, highScoresAggregate) -> highScoresAggregate.add(enriched),
      Materialized.<String, HighScores, KeyValueStore<Bytes,byte[]>>
        as(StreamConstants.LEADER_BOARDS_STORE)
        .withKeySerde(Serdes.String())
        .withValueSerde(JsonSerdes.HighScores())
    );

    highScores.toStream().to(StreamConstants.HIGH_SCORES_STREAM);
    highScores.toStream().print(Printed.<String,HighScores>toSysOut().withLabel("high-scores-stream"));

    //With Ktables, we need another type of aggregation (which includes a subtractor)
    final KGroupedTable<String, Player> groupedPlayers = players.groupBy(
      (key, value) -> KeyValue.pair(key, value),
      Grouped.with(Serdes.String(), JsonSerdes.Player())
    );


    final KTable<String, Long> playersCount = groupedPlayers.aggregate(
      () -> 0L,
      (k, v, aggregate) -> aggregate + 1L, //whenever a player enters the game, increase the count by 1
      (k, v, aggregate) -> aggregate - 1L //whenever a player leaves the game, decrease the count by 1
      , Materialized.with(Serdes.String(), Serdes.Long())
    );


    playersCount.toStream().to("players-count");
    playersCount.toStream().print(Printed.<String,Long>toSysOut().withLabel("players-count-stream"));


    return builder.build();

  }

}
