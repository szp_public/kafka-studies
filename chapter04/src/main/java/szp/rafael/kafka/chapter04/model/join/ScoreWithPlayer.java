package szp.rafael.kafka.chapter04.model.join;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import szp.rafael.kafka.chapter04.model.Player;

@Data(staticConstructor = "of")
@AllArgsConstructor
@Builder
public class ScoreWithPlayer {
  private ScoreEvent scoreEvent;
  private Player player;
}
