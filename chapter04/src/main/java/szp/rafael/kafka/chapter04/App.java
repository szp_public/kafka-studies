package szp.rafael.kafka.chapter04;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.state.HostInfo;
import szp.rafael.kafka.chapter04.service.LeaderboardService;
import szp.rafael.kafka.chapter04.stream.LeaderboardTopology;
import szp.rafael.kafka.common.config.StreamsConfiguration;

import java.io.File;
import java.util.Properties;

public class App {

  static final String ID_CONFIG = "chapter04-vg-leaderboard";


  public static void main(String[] args) {
    String host = System.getProperty("host", "localhost");
    Integer port = Integer.parseInt(System.getProperty("port", "9090"));
    String stateDir = System.getProperty("state_dir", System.getProperty("java.io.tmpdir")+ File.separator+ ID_CONFIG +"_"+port);
    String endpoint = String.format("http://%s:%s", host, port);

    final Properties config = StreamsConfiguration.properties(ID_CONFIG, null);
//    final Properties config = StreamsConfiguration.properties(ID_CONFIG, "earliest");
    config.put(StreamsConfig.APPLICATION_SERVER_CONFIG, endpoint);
    config.put(StreamsConfig.STATE_DIR_CONFIG, stateDir);

    final Topology topology = LeaderboardTopology.build();

    KafkaStreams streams = new KafkaStreams(topology, config);
    Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    streams.start();

    HostInfo hostInfo = new HostInfo(host, port);
    LeaderboardService service = new LeaderboardService(hostInfo,streams);
    service.start();

  }

}
