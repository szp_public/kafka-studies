package szp.rafael.kafka.chapter04.model.join;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ScoreEvent {
  private Long playerId;
  private Long productId;
  private Double score;
}
