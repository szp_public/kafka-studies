package szp.rafael.kafka.chapter04.model.join;

import lombok.Data;
import szp.rafael.kafka.chapter04.model.Product;

@Data
public class Enriched implements Comparable<Enriched> {
  private long playerId;
  private Long productId;
  private String playerName;
  private String gameName;
  private Double score;

  public Enriched(ScoreWithPlayer scoreWithPlayer, Product product) {
    this.playerId = scoreWithPlayer.getPlayer().getId();
    this.productId = product.getId();
    this.playerName = scoreWithPlayer.getPlayer().getName();
    this.gameName = product.getName();
    this.score = scoreWithPlayer.getScoreEvent().getScore();
  }

  @Override
  public int compareTo(Enriched o) {
    //Ordering by the greater score
    return Double.compare(o.score,score);
  }
}
