package szp.rafael.kafka.chapter04.serialization;

import org.apache.kafka.common.serialization.Serde;
import szp.rafael.kafka.chapter04.model.*;
import szp.rafael.kafka.chapter04.model.aggregation.HighScores;
import szp.rafael.kafka.chapter04.model.join.Enriched;
import szp.rafael.kafka.chapter04.model.join.ScoreEvent;
import szp.rafael.kafka.chapter04.model.join.ScoreWithPlayer;
import szp.rafael.kafka.common.serialization.json.JsonSerdeFactory;

public class JsonSerdes {

  public static Serde<ScoreEvent> ScoreEvent() {
    return new JsonSerdeFactory<ScoreEvent>().build(ScoreEvent.class);
  }
  public static Serde<Player> Player() {
    return new JsonSerdeFactory<Player>().build(Player.class);
  }
  public static Serde<Product> Product() {
    return new JsonSerdeFactory<Product>().build(Product.class);
  }

  public static Serde<ScoreWithPlayer> ScoreWithPlayer() {
    return new JsonSerdeFactory<ScoreWithPlayer>().build(ScoreWithPlayer.class);
  }

  public static Serde<Enriched> Enriched() {
    return new JsonSerdeFactory<Enriched>().build(Enriched.class);
  }

  public static Serde<HighScores> HighScores() {
    return new JsonSerdeFactory<HighScores>().build(HighScores.class);
  }

}
