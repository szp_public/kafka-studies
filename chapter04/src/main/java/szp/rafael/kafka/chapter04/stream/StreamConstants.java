package szp.rafael.kafka.chapter04.stream;

public class StreamConstants {

  public static final String SCORE_EVENTS_STREAM = "score-events";
  public static final String PLAYERS_STREAM = "players";
  public static final String PRODUCTS_STREAM = "products";
  public static final String HIGH_SCORES_STREAM = "high-scores";
  public static final String LEADER_BOARDS_STORE = "leader-boards";
}
