package szp.rafael.kafka.common.serialization.avro;

import org.apache.avro.specific.SpecificRecordBase;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

/*
 * Please use the original here: https://github.com/mitch-seymour/kafka-registryless-avro-serdes/tree/master/src/main/java/com/mitchseymour/kafka/serialization/avro
 * */
public class RegistrylessAvroSerdes {
  private RegistrylessAvroSerdes() {
    // do not allow instantiation
  }

  /*
   * Please use the original here: https://github.com/mitch-seymour/kafka-registryless-avro-serdes/tree/master/src/main/java/com/mitchseymour/kafka/serialization/avro
   * */
  public static <T extends SpecificRecordBase> Serde<T> get(Class<T> clazz) {
    RegistrylessAvroSerializer<T> serializer = new RegistrylessAvroSerializer<>(clazz);
    RegistrylessAvroDeserializer<T> deserializer = new RegistrylessAvroDeserializer<>(clazz);
    return Serdes.serdeFrom(serializer, deserializer);
  }
}
