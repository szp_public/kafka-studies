package szp.rafael.kafka.common.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;

import java.util.Properties;

public class StreamsConfiguration {

  public static final Properties properties(String idConfig) {
    return properties(idConfig, "earliest");
  }

  public static final Properties properties(String idConfig, String resetConfig) {
    Properties config = new Properties();
    config.put(StreamsConfig.APPLICATION_ID_CONFIG, idConfig);
    config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    if(resetConfig!=null) {
      config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, resetConfig);
    }
    config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Void().getClass());
    config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
    config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG, LogAndContinueExceptionHandler.class.getName());
    config.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
    return config;
  }

}
