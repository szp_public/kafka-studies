package szp.rafael.kafka.common.serialization.json;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public class JsonSerdeFactory<T> {

  public Serde<T> build(Class<T> clazz){
    JsonSerializer<T> serializer = new JsonSerializer<>();
    JsonDeserializer<T> deserializer = new JsonDeserializer(clazz);
    return Serdes.serdeFrom(serializer,deserializer);
  }

}
