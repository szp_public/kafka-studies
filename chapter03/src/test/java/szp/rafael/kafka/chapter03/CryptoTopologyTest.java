package szp.rafael.kafka.chapter03;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.test.TestRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import szp.rafael.kafka.chapter03.model.EntitySentiment;
import szp.rafael.kafka.chapter03.model.Tweet;
import szp.rafael.kafka.chapter03.model.serialization.json.TweetGsonSerdes;
import szp.rafael.kafka.common.serialization.avro.RegistrylessAvroSerdes;

import java.util.List;
import java.util.Properties;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class CryptoTopologyTest {

  TopologyTestDriver testDriver;
  TestInputTopic<byte[], Tweet> inputTopic;
  TestOutputTopic<byte[], EntitySentiment> outputTopic;


  @BeforeEach
  void setup() {
    Topology topology = CryptoTopology.build();

    Properties props = new Properties();
    props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "nono:1523");
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
    testDriver = new TopologyTestDriver(topology, props);

    inputTopic = testDriver.createInputTopic("tweets", Serdes.ByteArray().serializer(), new TweetGsonSerdes().serializer());

    outputTopic = testDriver.createOutputTopic("crypto-sentiment", Serdes.ByteArray().deserializer(), RegistrylessAvroSerdes.get(EntitySentiment.class).deserializer());
  }

  @Test
  void should_work() {
    Random random = new Random();
    Tweet tweet = new Tweet();
    tweet.setLang("en");
    tweet.setCreatedAt(System.currentTimeMillis());
    tweet.setText("This new cryptos Bitcoin and Ethereum may be cool");
    tweet.setId(Math.abs(random.nextLong()));
    tweet.setRetweet(false);

    inputTopic.pipeInput(new byte[]{}, tweet);
    assertFalse(outputTopic.isEmpty());

    List<TestRecord<byte[], EntitySentiment>> outRecords = outputTopic.readRecordsToList();
    assertEquals(2, outRecords.size());

  }


}