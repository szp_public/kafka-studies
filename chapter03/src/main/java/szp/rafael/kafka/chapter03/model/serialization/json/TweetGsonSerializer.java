package szp.rafael.kafka.chapter03.model.serialization.json;

import com.google.gson.Gson;
import org.apache.kafka.common.serialization.Serializer;
import szp.rafael.kafka.chapter03.model.Tweet;

import java.nio.charset.StandardCharsets;

public class TweetGsonSerializer implements Serializer<Tweet> {

  private Gson gson = new Gson();

  @Override
  public byte[] serialize(String topic, Tweet tweet) {
    if (tweet == null) return null;
    return gson.toJson(tweet).getBytes(StandardCharsets.UTF_8);
  }
}
