package szp.rafael.kafka.chapter03;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.Topology;
import szp.rafael.kafka.common.config.StreamsConfiguration;

public class Chapter03App {

  public static void main(String[] args) {
    Topology topology = CryptoTopology.build();
    KafkaStreams streams = new KafkaStreams(topology, StreamsConfiguration.properties("dev-chapter03"));
    Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    System.out.println("Starting Twitter streams");
    streams.start();
  }
}
