package szp.rafael.kafka.chapter03;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import szp.rafael.kafka.chapter03.language.LanguageClient;
import szp.rafael.kafka.chapter03.model.EntitySentiment;
import szp.rafael.kafka.chapter03.model.Tweet;
import szp.rafael.kafka.chapter03.model.serialization.avro.AvroSerdes;
import szp.rafael.kafka.chapter03.model.serialization.json.TweetGsonSerdes;

import java.util.Arrays;
import java.util.List;

public class CryptoTopology {

  private static final List<String> currencies = Arrays.asList("bitcoin", "ethereum");

  public static Topology build() {
    LanguageClient lc = new LanguageClient();
    StreamsBuilder builder = new StreamsBuilder();
    KStream<byte[], Tweet> stream = builder.stream("tweets", Consumed.with(Serdes.ByteArray(), new TweetGsonSerdes()));
    stream.print(Printed.<byte[], Tweet>toSysOut().withLabel("tweets-stream"));

    final KStream<byte[], Tweet> filtered = stream.filterNot((k, tweet) -> tweet.isRetweet()); //Dropping retweets

    Predicate<byte[], Tweet> englishTweets = (k, tweet) -> tweet.getLang().equals("en");
    Predicate<byte[], Tweet> nonEnglishTweets = (k, tweet) -> !tweet.getLang().equals("en");

    final KStream<byte[], Tweet>[] branches = filtered.branch(englishTweets, nonEnglishTweets);
    final KStream<byte[], Tweet> englishStream = branches[0];
    final KStream<byte[], Tweet> nonEnglishStream = branches[1];

    englishStream.print(Printed.<byte[], Tweet>toSysOut().withLabel("english tweets"));
    nonEnglishStream.print(Printed.<byte[], Tweet>toSysOut().withLabel("non-english tweets"));

    final KStream<byte[], Tweet> translatedStream = nonEnglishStream.mapValues((tweet) -> lc.translate(tweet, "en"));
    translatedStream.print(Printed.<byte[], Tweet>toSysOut().withLabel("translated tweets"));

    final KStream<byte[], Tweet> merged = englishStream.merge(translatedStream);

    //Every item in the List<EntitySentiment> will be injected into the enriched stream
    final KStream<byte[], EntitySentiment> enrichedStream = merged.flatMapValues((tweet) -> {
      final List<EntitySentiment> res = lc.getEntitySentiment(tweet);
      res.removeIf(e -> !currencies.contains(e.getEntity()));
      return res;
    });

    enrichedStream.to("crypto-sentiment", Produced.with(Serdes.ByteArray(), AvroSerdes.EntitySentiment("http://localhost:8081", false)));

    enrichedStream.print(Printed.<byte[], EntitySentiment>toSysOut().withLabel("crypto-sentiment"));

    return builder.build();
  }

}
