package szp.rafael.kafka.chapter03.language;

import szp.rafael.kafka.chapter03.model.EntitySentiment;
import szp.rafael.kafka.chapter03.model.Tweet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LanguageClient {

  Random random = new Random();

  //Mock
  public Tweet translate(Tweet tweet, String targetLanguage) {
    Random rand = new Random();
    final List<String> ent = List.of("ethereum", "bitcoin");
    tweet.setText("Translated: the book is on the table " + tweet.getText() + " " + ent.get(rand.nextInt(ent.size())));
    tweet.setLang(targetLanguage);
    return tweet;
  }

  //Mock
  public List<EntitySentiment> getEntitySentiment(Tweet tweet) {
    List<EntitySentiment> ents = new ArrayList<>();

    final List<String> words = List.of(tweet.getText().toLowerCase().replaceAll("#", "").split("\\s"));
    words.forEach(word -> ents.add(EntitySentiment.newBuilder()
      .setSalience(random.nextDouble())
      .setSentimentMagnitude(random.nextDouble())
      .setSentimentScore(random.nextDouble())
      .setCreatedAt(tweet.getCreatedAt())
      .setText(tweet.getText())
      .setId(tweet.getId())
      .setEntity(word)
      .build()
    ));
    return ents;

  }

}
