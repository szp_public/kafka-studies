package szp.rafael.kafka.chapter03.model.serialization.json;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import szp.rafael.kafka.chapter03.model.Tweet;

public class TweetGsonSerdes implements Serde<Tweet> {

  public TweetGsonSerdes() {
  }

  @Override
  public Serializer<Tweet> serializer() {
    return new TweetGsonSerializer();
  }

  @Override
  public Deserializer<Tweet> deserializer() {
    return new TweetGsonDeserializer();
  }
}
