package szp.rafael.kafka.chapter03.model.serialization.avro;

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.common.serialization.Serde;
import szp.rafael.kafka.chapter03.model.EntitySentiment;

import java.util.Collections;
import java.util.Map;

public class AvroSerdes {

  public static Serde<EntitySentiment> EntitySentiment(String registryUrl, boolean isKey) {
    Map<String, String> serdeConfig = Collections.singletonMap("schema.registry.url", registryUrl);
    Serde<EntitySentiment> serde = new SpecificAvroSerde<>();
    serde.configure(serdeConfig, isKey);
    return serde;
  }

}
