## Chapter 03

## Reqs scripts

```shell
#Generate new EntitySentiment with avro-maven-plugin
mvn compile -DskipTests
```

```shell
docker exec -it broker bash
kafka-topics \
  --bootstrap-server localhost:9092 \
  --topic tweets \
  --replication-factor 1 \
  --partitions 4 \
  --create
```

then

```shell
kafka-topics \
  --bootstrap-server localhost:9092 \
  --topic crypto-sentiment \
  --replication-factor 1 \
  --partitions 4 \
  --create
```

### Testing

First, check topic

```shell
docker exec -it broker kafka-topics --bootstrap-server localhost:9092 --describe --topic tweets 
```

```shell
docker cp chapter03/src/test/data/test.json broker:/tmp/test.json
docker exec -it broker bash
kafka-console-producer --bootstrap-server localhost:9092 --topic tweets < /tmp/test.json
```

Then you can read the crypto-sentiment topic with

```shell
kafka-console-consumer --bootstrap-server localhost:9092 --topic crypto-sentiment --from-beginning
```